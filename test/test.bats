#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/fastlane-pipe"}

#  echo "Building image..."
#  docker build -t ${DOCKER_IMAGE}:test .
}

@test "Dummy test" {
#    run docker run \
#        -e LANE="alpha"
#        -e GOOGLE_PLAY_KEY="${GOOGLE_PLAY_KEY}"
#        -e KEY_ALIAS="${KEY_ALIAS}" \
#        -e KEY_PASSWORD="${KEY_PASSWORD}" \
#        -e STORE_PASSWORD="${STORE_PASSWORD}" \
#        -e KEYSTORE="${KEYSTORE}"
#        -v $(pwd):$(pwd) \
#        -w $(pwd) \
#        ${DOCKER_IMAGE}:test

    run docker build -t ${DOCKER_IMAGE}:test .
    
    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}


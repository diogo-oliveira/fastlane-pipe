#!/usr/bin/env bash

source "$(dirname "$0")/common.sh"

enable_debug

info "iniciando Fastlane ..."

# PARAMETROS OBRIGATORIOS
LANE=${LANE:?'LANE não informado.'}
GOOGLE_PLAY_KEY=${GOOGLE_PLAY_KEY:?'GOOGLE_PLAY_KEY não informado.'}

debug LANE: "${LANE}"
debug GOOGLE-PLAY-KEY: "${GOOGLE_PLAY_KEY}"

KEYSTORE=${KEYSTORE:?'KEYSTORE não informado.'}
KEY_ALIAS=${KEY_ALIAS:?'KEY_ALIAS não informado.'}
KEY_PASSWORD=${KEY_PASSWORD:?'KEY_PASSWORD não informado.'}
STORE_PASSWORD=${STORE_PASSWORD:?'STORE_PASSWORD não informado.'}
KEYSTORE_PATH=${KEYSTORE_PATH:="${BITBUCKET_CLONE_DIR}/keystore.jks"}

debug KEYSTORE: "${KEYSTORE}"
debug KEY-ALIAS: "${KEY_ALIAS}"
debug KEY-PASSWORD: "${KEY_PASSWORD}"
debug STORE-PASSWORD: "${STORE_PASSWORD}"
debug KEYSTORE-PATH: "${KEYSTORE_PATH}"

debug OPTIONS: "${OPTIONS}"

export KEYSTORE_PATH="${KEYSTORE_PATH}"
echo "${KEYSTORE}" | base64 --decode >> "${KEYSTORE_PATH}"
echo "${GOOGLE_PLAY_KEY}" | base64 --decode >> credentials.json

run bundle install
#run bundle update fastlane
run bundle exec fastlane ${LANE} ${OPTIONS}

if [[ "${status}" == "0" ]]; then
  success "Fastlane executado com sucesso!"
else
  fail "Fastlane falhou! :("
fi

# Bitbucket Pipelines Pipe: Fastlane

Este pipe publica o aplicativo no Google Play

## Definição de YAML

Adicione o seguinte trecho à seção de script do seu arquivo `bitbucket-pipelines.yml`:

```yaml
script:
  - pipe: diogo-oliveira/fastlane-pipe:1.0.0
    variables:
      LANE: "<string>"
      GOOGLE_PLAY_KEY: "<string>"
      KEYSTORE: "<string>"
      KEY_ALIAS: "<string>"
      KEY_PASSWORD: "<string>"
      STORE_PASSWORD: "<string>"
      KEYSTORE_PATH: "<string>"
      # OPTIONS: "<string>" # Optional
      # SLACK_URL: "<string>" # Optional
      # DEBUG: "<boolean>" # Optional
```
## Variáveis

| Variável Uso             | Descrição                                                                                                   |   
| ------------------------ | ----------------------------------------------------------------------------------------------------------- |
| LANE (*)                 | Lane que será executado                                                                                      |
| GOOGLE_PLAY_KEY (*)      | Json key de autorização do Google Play codificado em base64                                                 |
| KEYSTORE (*)             | Keystore de assinatura da aplicação codificado em base64                                                    |
| KEY_ALIAS (*)            | Alias da keystore                                                                                           |
| KEY_PASSWORD (*)         | Senha definida no keystore. É recomendável usar uma variável segura de repositório.                         |
| STORE_PASSWORD (*)       | Senha da keystore. É recomendável usar uma variável segura de repositório.                                  |
| KEYSTORE_PATH            | Local da keystore. Padrão: `"${BITBUCKET_CLONE_DIR}/keystore.jks"`                                          |
| OPTIONS                  | Parametros para o lane. Padrão key:value: `param1:<value> param2:<value>`                                   |
| SLACK_URL                | Webhook para notificação no Slack. É recomendável usar uma variável segura de repositório.                  |
| DEBUG                    | Ativar informações extras de depuração. Padrão: `false`.                                                    |

_ (*) ​​= variável necessária._

## Pré-requisitos

## Exemplos

Exemplo básico:

```yaml
script:
  - pipe: diogo-oliveira/fastlane-pipe:1.0.0
    variables:
      LANE: "alpha"
      GOOGLE_PLAY_KEY: "${GOOGLE_PLAY_KEY}"
      KEYSTORE: "${KEYSTORE}"
      KEY_ALIAS: "${KEY_ALIAS}" 
      KEY_PASSWORD: "${KEY_PASSWORD}" 
      STORE_PASSWORD: "${STORE_PASSWORD}"
      OPTIONS: "flavor:demo"
```

## Apoio, suporte
Se você precisar de ajuda com este canal ou tiver um problema ou solicitação de recurso, informe-nos.
O tubo é mantido por diogo0liveira@hotmail.com.

Se você estiver relatando um problema, inclua:

- a versão do pipe
- logs e mensagens de erro relevantes
- Passos para reproduzir
